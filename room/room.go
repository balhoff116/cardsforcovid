package room

import (
	"encoding/json"

	"bitbucket.org/balhoff116/cardsforcovid/client"
	"bitbucket.org/balhoff116/cardsforcovid/game"
	msg "bitbucket.org/balhoff116/cardsforcovid/message"
)

const defaultRoomID = "1"

type Room struct {
	Clients   map[*client.Client]bool
	Chans     client.ClientChans
	broadcast chan msg.BroadcastData
	instance  *game.GameInstance
}

func NewRoom(pieces *game.GamePieces, broadcast chan msg.BroadcastData) Room {
	return Room{
		Clients:   make(map[*client.Client]bool),
		Chans:     client.NewClientChans(), //TODO: move NewClientChans into room
		broadcast: broadcast,
		instance:  game.NewGameInstance(pieces),
	}
}

func (r Room) Run() {
	for {
		select {
		case reg := <-r.Chans.Register:
			r.handleRegister(reg)
		case reg := <-r.Chans.Unregister:
			r.handleUnregister(reg)
		case nameReq := <-r.Chans.Name:
			r.setPlayerName(&nameReq.Client, &nameReq.Payload)
		case proposalReq := <-r.Chans.Propose:
			r.setProposal(&proposalReq.Client, &proposalReq.Payload)
		case winnerReq := <-r.Chans.Submit:
			r.setWinner(&winnerReq.Client, &winnerReq.Payload)
		}
	}
}

func (r *Room) handleRegister(data client.RegisterData) {
	room := r.Clients
	room[data.Client] = true
	r.initNewPlayer(data.Client)
}

func (r *Room) handleUnregister(data client.RegisterData) {
	room := r.Clients
	if _, ok := room[data.Client]; ok {
		r.deletePlayer(data.Client)
		delete(room, data.Client)
		close(data.Client.Send) //TODO: threading issues??
	}
}

func (r *Room) initNewPlayer(client *client.Client) {
	player := game.NewPlayer(client.ID, r.instance)

	// Place new clients during judging in inactive state
	if r.instance.IsJudgeTime() {
		player.Active = false
	}

	if len(r.instance.Players) == 0 {
		player.IsJudging = true
	}

	r.instance.AddPlayer(player)

	r.sendInitMessage(client)
	go r.broadcastState()
}

func (r *Room) deletePlayer(client *client.Client) {
	r.instance.DeletePlayer(r.findPlayer(client))
	go r.broadcastState()
}

func (r *Room) findPlayer(client *client.Client) (int, *game.Player) {
	for ix, player := range r.instance.Players {
		if player.ClientId == client.ID {
			return ix, player
		}
	}

	return 0, nil
}

func (r *Room) broadcastState() {
	stateMessage := msg.NewStateMessage(r.instance)

	bytes, err := json.Marshal(stateMessage)

	if err != nil {
		//todo
	}

	r.broadcast <- msg.BroadcastData{
		RoomID:  defaultRoomID,
		Payload: bytes,
	}
}

//TODO: move to hub??
func (r *Room) sendInitMessage(client *client.Client) {
	initMessage := msg.NewInitClientMessage(client.ID, r.instance.GamePieces)

	bytes, err := json.Marshal(initMessage)

	if err != nil {
		//todo. disconnect the client
	}

	//TODO: should this be done like this, or sent through the hub?
	client.Send <- bytes
}

func (r *Room) setPlayerName(client *client.Client, payload *msg.SetNameObj) {
	r.instance.SetPlayerName(client.ID, payload.Name)
	r.broadcastState()
}

func (r *Room) setProposal(client *client.Client, payload *msg.SubmitProposalObj) {
	//if the round is not the same, the judge must have been dropped
	//that's how i'm detecting an en route proposal after dropping a judge
	if r.instance.Round == payload.Round {
		_, player := r.findPlayer(client)
		player.CurrentProposal = payload.CardIds
		r.broadcastState()
	}
}

func (r *Room) setWinner(client *client.Client, payload *msg.SubmitWinnerObj) {
	//player := h.findPlayer(client)

	//set the last turn
	//+1 the score
	//nil all the current proposals
	//delete cards that were used, deal new ones out
	//  multiple draws
	//  need to know current outstanding cards
	//move the judge over one
	//  need an ordering to the clients to do this
	//    i might cheat and sort by uuid
	//increase the round
	r.instance.NextRound(payload.PlayerId)
	r.broadcastState()
}
