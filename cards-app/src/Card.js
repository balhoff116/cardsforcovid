import React from 'react';
import {shuffle} from "lodash";

function Card({className, onClick, cardId, cardText, cssId, selected}) {
  const isSelected = selected && selected.includes(cardId)
  return (
    <div className={className} id={cssId} onClick={onClick} data-sel={isSelected}>
      <div className="cardtext" dangerouslySetInnerHTML={{__html: cardText}} />
    </div>
  )
}

export function BlackCard({cssId, whiteCards, blackCard, onClick}) {
  return (
    <Card
      className="blackcard gamecard"
      cssId={cssId}
      onClick={onClick}
      cardText={convertBlackText(whiteCards,blackCard)}
    />
  )
}

export function WhiteCardHand({whiteCards, onClick, selected}) {
  let cards = []
  for (const card of whiteCards) {
    cards.push(
      <Card
        key={card.id}
        className="whitecard gamecard"
        onClick={() => onClick(card.id)}
        cardId = {card.id}
        cardText={convertWhiteText(card)}
        selected={selected}
      />
    )
  }
  return cards
}

export function BlackCardHand({players, playerNames, blackCard, onClick, whites, isPrevious, winnerIndex}) {
  let cards = []
  let whiteCards = []
  const className = isPrevious ? "previousProposal" : "currentProposal"
  for (let ix = 0; ix < players.length; ix++) {
    const player = players[ix]
    const playerName = playerNames[player.clientId]
    const proposal = isPrevious ? player.whiteCardIds : player.currentProposal
    const isWinner = ix === winnerIndex
    if (proposal && proposal.length) {
      whiteCards = getProposal(proposal, whites)
      cards.push(
        <div
          key={player.clientId}
          className={className}
          data-winner={isWinner}
        >
          <p className="cardowner">{playerName}</p>
          <BlackCard
            blackCard={blackCard}
            whiteCards={whiteCards}
            onClick={() => onClick(player.clientId)}
          />
        </div>
      )
    }
  }
 return shuffle(cards)
}

function getProposal(currProposal,whites) {
  const proposal = []
  for (const whiteCard of currProposal) {
    proposal.push(whites.get(whiteCard))
  }
  return proposal
}

function convertWhiteText(whiteCard) {
  let newText = convertToUnicode(whiteCard.text)
  newText = stripEndingPeriod(newText)
  return newText
}

function convertBlackText(whiteCards, blackCard) {
  const cardText = blackCard.text
  const convertedBlackText = convertToUnicode(cardText)
  let convertedWhiteTexts = []
  for (let ix = 0; ix < whiteCards.length; ix++) {
    let newText = convertToUnicode(whiteCards[ix].text)
    newText = stripEndingPeriod(newText)
    convertedWhiteTexts.push(newText)
  }
  const combinedCardItems = combineWhiteAndBlackCards(convertedBlackText, convertedWhiteTexts)

  return combinedCardItems
}

function stripEndingPeriod(cardText) {
  if (cardText.length > 0 && cardText[cardText.length - 1] === '.') {
    return cardText.slice(0, -1)
  }
  return cardText
}

//Inserts the white cards' texts into the black card and underlines the white text.
function combineWhiteAndBlackCards(blackCardText, whiteCardTexts) {
  let blackCardPieces = blackCardText.split("_")

  let updatedText = ""
  let i = 0;
  for (; i < whiteCardTexts.length; i++) {
    //if there are black card pieces left, add this one
    if (i < blackCardPieces.length) {
      updatedText += blackCardPieces[i]
    }

    //We're at least at the last black card piece. No more underscores,
    //  so we need to add a break before adding the white card text.
    if (i >= blackCardPieces.length - 1) {
      updatedText += "<br>"
    }

    updatedText += "<u>" + whiteCardTexts[i] + "</u>"
  }

  //in case not all white cards have been chosen
  //also puts the last section of black card text in in normal cards
  if (i < blackCardPieces.length) {
    updatedText += blackCardPieces.slice(i).join('_')
  }

  return updatedText
}

//convertToUnicode returns a string that has replaced certain
//html escaped strings with their unicode equivalent, to get around
//jsx double escaping
function convertToUnicode(cardText) {
  const unicode = [
    [/&reg;/g, "\u00AE"],
    [/&iacute;/g, "\u00ED"],
    [/&amp;/g, "\u0026"],
    [/&trade;/g, "\u2122"],
    [/&Uuml;/g, "\u00DC"],
    [/&ntilde;/g, "\u00F1"],
    [/&eacute;/g, "\u00E9"],
    [/<[^<]*br.*?>/g, "<br>"], //open br tags. non-greedy select of >. does not handle <abbr>
    [/<[^<]*\/i.*?>/g, "<em>"], //close italic tag
    [/<[^<]*i.*?>/g, "</em>"] //open italic tag
  ]

  //console.log(unicode)
  for (const elem of unicode) {
    cardText = cardText.replace(elem[0], elem[1])
  }

  return cardText
}

export default BlackCard;
