import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Server from "./Server";
import 'bootstrap/dist/css/bootstrap.min.css';

const server = new Server();

ReactDOM.render(
  <React.StrictMode>
    <App server={server}/>
  </React.StrictMode>,
  document.getElementById('root')
);
