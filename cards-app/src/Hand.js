import React from 'react';
import Card from 'react-bootstrap/Card';

export default function Hand({cards, onClick, activeId}) {
    let cardViews = []
    for (const card of cards) {
      const border = card.id === activeId ? "primary" : "none"
      cardViews.push(
        <Card key={card.id} border={border} onClick={() => onClick(card.id)} body>
          {/* TODO: remove this / find a different way */}
          <div dangerouslySetInnerHTML={{__html: card.text}} />
        </Card>
      )
    }
    return <div>
      {cardViews}
    </div>
}
