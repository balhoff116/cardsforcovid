import React from 'react';

export default function ScoreBoard({players, round, myClientId, myName}) {
  return (
    <table id="scoreboard">
      <thead><tr><th id="scoreboardtitle" colSpan="2">Round {round}</th></tr></thead>
      <tbody>{Scores({players, myClientId, myName})}</tbody>
    </table>
  )
}

function Scores({players, myClientId, myName}) {
  let scores = []
  for (const player of players) {
    const isMe = player.clientId === myClientId
    const pName = isMe ? myName : player.name
    const submitted = player.isJudging || player.currentProposal !== null
    scores.push(
      <tr className="player" key={player.clientId}>
        <td className="name" data-judge={player.isJudging} data-submitted={submitted} data-me={isMe}>{pName}</td>
        <td className="score">{player.score}</td>
      </tr>
    )
  }
  return scores
}