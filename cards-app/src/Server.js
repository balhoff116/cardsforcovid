export default class Server extends EventTarget {
  constructor() {
    super()
    this.first = true
    this.apiLocation = process.env.NODE_ENV !== 'production' ? 'localhost:8090' : window.location.host
    const store = window.localStorage
    this.localStore = store

    this.clientId = store.getItem("clientId") || ""
    this.userName = store.getItem("userName") || ""
  }

  initWs() {
    if(this.first) {
      this.first = false
    } else {
      return
    }

    console.log("Init sockets")
    const secure = window.location.protocol === "https:";
    const wsProto = secure ? "wss" : "ws";
    this.conn = new WebSocket(wsProto + "://" + this.apiLocation + "/ws");
    this.conn.onclose = (evt) => {
        console.log(evt);
    };
    this.conn.onmessage = (evt) => {
      const {command, payload} = JSON.parse(evt.data);
      const event = new Event(command)
      event.payload = payload;
      this.dispatchEvent(event);
    };
  }

  updateUsername(name) {
    if(name !== this.userName) {
      this.localStore.setItem("userName", name)
      this.setName(name)
    }
  }

  setName(name) {
    const data = {
      command: "setName",
      payload: {name}
    }
    this.conn.send(JSON.stringify(data))
  }

  proposeCard(round, cardIds) {
    const data = {
      command: "submitProposal",
      payload: {round, cardIds}
    }
    this.conn.send(JSON.stringify(data))
  }

  submitWinner(round, playerId) {
    const data = {
      command: "submitWinner",
      payload: {round, playerId}
    }
    this.conn.send(JSON.stringify(data))
  }

  //just moving the cards to maps from id -> card because it's
  //way easier to use that way
  static translateInitState({clientId, gamePieces: {whites, blacks}}) {
    let cardGame = {}
    cardGame.clientId = clientId
    cardGame.whites = new Map()
    cardGame.blacks = new Map()

    for (const card of whites) {
      cardGame.whites.set(card.id, card)
    }

    for (const card of blacks) {
      cardGame.blacks.set(card.id, card)
    }

    return cardGame
  }
}
