import React from 'react';
import './App.css';
import Server from "./Server";
import {BlackCard, WhiteCardHand, BlackCardHand} from "./Card";
import ScoreBoard from "./Scoreboard"
import './Card.css';
import './Scoreboard.css';

class App extends React.Component {
  constructor(props) {
    super(props)
    const {server} = props;
    this.server = server;
    server.initWs();

    const name = server.userName || ""

    this.state = {
      gameState: {},
      cardGame: {},
      userNameInput: name,
      userEditing: name === "",
      selectedCards: [],
      currentRound: -1,
      previousRound: -1,
    }
  }

  componentDidMount() {
    console.log("Setup events")
    this.server.addEventListener('state', ({payload}) => {
      console.log('State updated', payload);

      let newSelectedCards = this.state.selectedCards
      let newCurrentRound = this.state.currentRound

      if (payload.round !== newCurrentRound) {
        newSelectedCards = []
        newCurrentRound = payload.round
      }

      this.setState({gameState: payload, selectedCards: newSelectedCards, currentRound: newCurrentRound})
    });
    this.server.addEventListener('init', ({payload}) => {
      console.log('State init', payload);
      this.setState({cardGame: Server.translateInitState(payload)})
      // Trigger push of name to server
      if(this.server.userName !== "") {
        this.server.setName(this.server.userName)
      }
    });
  }

  onNameUpdate(e) {
    this.setState({userNameInput: e.target.value})
  }

  onNameChange(e) {
    e.preventDefault();
    this.server.updateUsername(this.state.userNameInput)
    this.setState({userEditing: false})
  }

  onJudge(winnerId){
  }

  isJudge() {
    return this.findCurrentPlayer().isJudging;
  }

  onPropose(cardId) {
    console.log("Card selected", cardId)
    const selectedCards = this.state.selectedCards
    const blanks = this.getCurrentBlackCard().blanks
    const iAmJudge = this.isJudge();
    if(selectedCards.indexOf(cardId) > -1 || selectedCards.length >= blanks || iAmJudge) {
      return
    }
    selectedCards.push(cardId)
    this.setState({selectedCards})
    if (blanks === selectedCards.length) {
      this.server.proposeCard(this.state.gameState.round, selectedCards)
    }
  }

  onSelectWinner(winnerId) {
    const iAmJudge = this.findCurrentPlayer().isJudging;
    if (iAmJudge) {
      this.server.submitWinner(this.state.gameState.round, winnerId)
    }
  }

  selectedCards() {
    const whiteCards = []
    for (const card of this.state.selectedCards) {
      whiteCards.push(this.state.cardGame.whites.get(card))
    }
    return whiteCards
  }

  isTimeToJudge(players) {
    for (const player of players) {
      if (!player.isJudging && !player.currentProposal) {
        return false
      }
    }
    return true
  }
  
  onNullClick(playerId) {
  }
  
  renderInstructionText(timeToJudge) {
    const iAmJudgeText = "You are the Judge."
    const iAmJudgeWaitText = "Please wait while players make their selections."
    const iAmJudgeSelectText = "Please select a winner."

    const blanks = this.getCurrentBlackCard().blanks

    const cardText = blanks === 1 ? "card" : "cards"
    const iAmPlayerText = "You are a Player."
    const iAmPlayerWaitText = "Please wait while the Judge chooses a winner."
    const iAmPlayerSelectText = "Please choose " + blanks + ` white ${cardText}.`
    
    const title = this.isJudge() ? iAmJudgeText : iAmPlayerText
    let subtitle = ""
    if (this.isJudge()) {
      subtitle = timeToJudge ? iAmJudgeSelectText : iAmJudgeWaitText
    } else {
      subtitle = timeToJudge ? iAmPlayerWaitText : iAmPlayerSelectText
    }
    
    return (
      <div className="line">
        <p className="title">{title}</p>
        <br />
        <p className="subtitle">{subtitle}</p>
      </div>
    )
  }
  
  renderPreviousHand() {
    let round = this.state.previousRound
    if (round === -1) {
      if (!this.state.gameState.previousRounds) {
        return
      }
      round = this.state.gameState.previousRounds.length - 1
    }
    const roundText = "Round " + round
    const {blackCardId, proposals, winnerIndex} = this.state.gameState.previousRounds[round]
    return (
      <div className="outer">
        <div className="line">
          <p className="title">{roundText}</p>
        </div>
        <div className="hand">
          <BlackCardHand
            className="blackcard"
            blackCard={this.state.cardGame.blacks.get(blackCardId)}
            onClick={(e) => this.onNullClick(e)}
            players={proposals}
            playerNames={this.state.gameState.playerNames}
            whites={this.state.cardGame.whites}
            isPrevious="true"
            winnerIndex={winnerIndex}
          />
        </div>
      </div>
    )
  }

  render() {
    const {userNameInput, userEditing} = this.state;
    const {clientId, whites} = this.state.cardGame;
    const {players, round} = this.state.gameState;
    console.log("State", this.state)

    const loaded = round >= 0
    const timeToJudge = loaded && this.isTimeToJudge(players)
    const judgeHandClassName = loaded &&
      this.isJudge() ? "hand judging" : "hand"

    const nameDisplay = <div onClick={() => this.setState({userEditing: !userEditing})}>
      {userNameInput}
    </div>

    return (
      <div id="App">
        <header id="App-header">
          {userEditing &&
            <NameForm
              name={userNameInput}
              onChange={(e) => this.onNameUpdate(e)}
              onSubmit={(e) => this.onNameChange(e)}
            />
          }
        </header>
        <div id="main">
          <div id="game">
            <div className="line" />
            <div id="round-header">
              {loaded &&
                <BlackCard
                  cssId="currentcard"
                  blackCard={this.getCurrentBlackCard()}
                  whiteCards={this.selectedCards()}
                />
              }
              {loaded &&
                <ScoreBoard
                  players={players}
                  round={round}
                  myClientId={clientId}
                  myName={nameDisplay}
                />
              }
            </div>
            {loaded && this.renderInstructionText(timeToJudge)}
            {loaded && <div className={judgeHandClassName}>
              {timeToJudge &&
                <BlackCardHand
                  className="blackcard"
                  blackCard={this.getCurrentBlackCard()}
                  onClick={(e) => this.onSelectWinner(e)}
                  players={players}
                  playerNames = {this.state.gameState.playerNames}
                  whites={whites}
                />
              }
              {!timeToJudge &&
                <WhiteCardHand
                  className="whitecard"
                  whiteCards={this.getCurrentHand()}
                  onClick={(e) => this.onPropose(e)}
                  selected={this.state.selectedCards}
                />
              }
            </div>}
            {loaded && this.renderPreviousHand()}
          </div>
        </div>
      </div>
    );
  }

  findCurrentPlayer() {
    for (const player of this.state.gameState.players) {
      if (player.clientId === this.state.cardGame.clientId) {
        return player
      }
    }
    return null
  }

  getCurrentHand() {
    const whiteCards = []
    const currentPlayer = this.findCurrentPlayer()
    for (const card of currentPlayer.cards) {
      whiteCards.push(this.state.cardGame.whites.get(card))
    }
    return whiteCards
  }

  getCurrentBlackCard() {
    const cardId = this.state.gameState.blackCardId
    return this.state.cardGame.blacks.get(cardId)
  }
}

function NameForm({name, onChange, onSubmit}) {
  return <form onSubmit={onSubmit}>
    <input id="name-input" type="text" placeholder="Username" value={name} onChange={onChange}/>
    <input type="submit" value="Submit" />
  </form>
}

export default App;
