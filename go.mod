module bitbucket.org/balhoff116/cardsforcovid

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.2
	github.com/pelletier/go-toml v1.7.0
)
