package hub

import (
	"bitbucket.org/balhoff116/cardsforcovid/game"
	msg "bitbucket.org/balhoff116/cardsforcovid/message"
	"bitbucket.org/balhoff116/cardsforcovid/room"
)

type clientRooms map[string]room.Room

type Hub struct {
	clientRooms clientRooms
	broadcast   chan msg.BroadcastData
	gamePieces  *game.GamePieces
}

func NewHub(gamePieces *game.GamePieces) *Hub {
	return &Hub{
		clientRooms: make(clientRooms),
		broadcast:   make(chan msg.BroadcastData),
		gamePieces:  gamePieces,
	}
}

func (h *Hub) GetRoom(id string) room.Room {
	if _, ok := h.clientRooms[id]; !ok {
		h.clientRooms[id] = room.NewRoom(h.gamePieces, h.broadcast)
		go h.clientRooms[id].Run() //TODO: how do rooms get deleted?
	}
	return h.clientRooms[id]
}

func (h *Hub) Run() {
	for {
		select {
		case msg := <-h.broadcast:
			h.handleBroadcast(msg)
		}
	}
}

func (h *Hub) handleBroadcast(data msg.BroadcastData) {
	room := h.GetRoom(data.RoomID).Clients
	for client := range room {
		select {
		case client.Send <- data.Payload:
		default:
			//todo: can i not just unregister?
			close(client.Send)
			delete(room, client)
		}
	}
}
