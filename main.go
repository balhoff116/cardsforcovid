package main

import (
	"bufio"
	"log"
	"net/http"
	"os"

	"bitbucket.org/balhoff116/cardsforcovid/client"
	"bitbucket.org/balhoff116/cardsforcovid/game"
	"bitbucket.org/balhoff116/cardsforcovid/hub"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/pelletier/go-toml"
)

const defaultRoomID = "1"

type Config struct {
	CardPath  string `default:"cards/cards.json"`
	Port      string `default:"8090"`
	StaticDir string `default:"public/"`
}

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			// TODO better restriction out of dev
			return true
		},
	}
)

func loadConfig(path string) (config Config) {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal("Can't open config file")
	}

	decoder := toml.NewDecoder(bufio.NewReader(f))
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatal("Can't load config")
	}
	return config
}

func main() {
	config := loadConfig("config.toml")
	port := config.Port
	if v := os.Getenv("PORT"); v != "" {
		port = v
	}

	log.Println("Starting on port ", port)

	gamePieces, err := game.NewGamePieces(config.CardPath)
	if err != nil {
		log.Fatal("Fatal loading gamepieces: ", err)
	}

	hub := hub.NewHub(gamePieces)
	go hub.Run()

	http.Handle("/", http.FileServer(http.Dir(config.StaticDir)))
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("Fatal ListenAndServe: ", err)
	}
}

func serveWs(hub *hub.Hub, w http.ResponseWriter, r *http.Request) {
	uuid, err := uuid.NewRandom()
	if err != nil {
		log.Println(err)
		return
	}

	roomID := r.URL.Query().Get("room")
	if roomID == "" {
		roomID = defaultRoomID
	}
	log.Println("Room id", roomID)

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	room := hub.GetRoom(roomID)
	cl := &client.Client{
		Chans: room.Chans,
		Conn:  conn,
		Send:  make(chan []byte, 1024),
		ID:    uuid,
	}

	room.Chans.Register <- client.RegisterData{
		RoomID: defaultRoomID,
		Client: cl,
	}

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	cl.Start()
}
