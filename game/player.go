package game

import "github.com/google/uuid"

type Player struct {
	Name              string    `json:"name"`
	Cards             []int     `json:"cards"` //ids populated by the system
	ClientId          uuid.UUID `json:"clientId"`
	Score             int       `json:"score"`
	CurrentProposal   []int     `json:"currentProposal"` //ids of the current proposal
	IsJudging         bool      `json:"isJudging"`
	IsLastRoundWinner bool      `json:"isLastRoundWinner"`
	Active            bool      `json:"active"`
}

func NewPlayer(clientId uuid.UUID, instance *GameInstance) *Player {
	//TODO: move to the game instance
	//deal cards to the player's hand
	var cards []int
	for ix := 0; ix < instance.GamePieces.BlacksMap[instance.BlackCardId].GetHandSize(); ix++ {
		cards = append(cards, instance.whiteCardDeck.drawCard())
	}

	return &Player{
		Name:              "",
		Cards:             cards,
		ClientId:          clientId,
		Score:             0,
		CurrentProposal:   nil,
		IsJudging:         false,
		IsLastRoundWinner: false,
		Active:            true,
	}
}

func (p *Player) nextRound(winnerId uuid.UUID, blackCardId int, whiteCards *whiteCardDeck, gamePieces *GamePieces) {
	//set the last turn
	p.Active = true
	p.IsLastRoundWinner = p.ClientId == winnerId
	if p.IsLastRoundWinner {
		p.Score++
	}
	//discard used cards
	p.discardProposal(whiteCards, p.CurrentProposal)
	//init the current turn
	p.CurrentProposal = nil
	//draw new cards (or discard them)
	p.adjustHand(blackCardId, whiteCards, gamePieces)
}

//set the hand to the correct size for the next round
func (p *Player) adjustHand(blackCardId int, whiteCards *whiteCardDeck, gamePieces *GamePieces) {
	totalNewHandSize := gamePieces.BlacksMap[blackCardId].GetHandSize()
	if p.IsJudging {
		//this prevents judges from getting dealt 12 cards for cards with 3 blanks
		//another option is to make sure that the judge has at least 10 cards
		//  it's possible for a judge to have 12 cards because of a dropped round,
		//  have them removed by this logic, and then end up next round in a round with
		//  a black card that requires 12 white cards again. he could have kept the old ones.
		//  that logic is slightly more complicated so i decided not to implement it yet
		totalNewHandSize = baseHandCount
	}
	if totalNewHandSize >= len(p.Cards) {
		neededCardCount := totalNewHandSize - len(p.Cards)
		for ix := 0; ix < neededCardCount; ix++ {
			whiteCard := whiteCards.drawCard()
			p.Cards = append(p.Cards, whiteCard)
		}
	} else {
		//fringe scenarios. for instance, a judge drops during a round with 12 cards,
		//  that card is discarded, and everyone needs to be reset to 10
		for ix := totalNewHandSize; ix < len(p.Cards); ix++ {
			whiteCards.discardCard(p.Cards[ix])
			p.removeCardFromHand(p.Cards[ix])
		}
	}
}

func (p *Player) discardProposal(whiteCards *whiteCardDeck, whiteCardIds []int) {
	if whiteCardIds != nil {
		for _, usedCard := range whiteCardIds {
			whiteCards.discardCard(usedCard)
			p.removeCardFromHand(usedCard)
		}
	}
}

func (p *Player) removeCardFromHand(cardId int) {
	var cardIx int
	for ix, id := range p.Cards {
		if id == cardId {
			cardIx = ix
			break
		}
	}

	p.Cards[len(p.Cards)-1], p.Cards[cardIx] = p.Cards[cardIx], p.Cards[len(p.Cards)-1]
	p.Cards = p.Cards[:len(p.Cards)-1]
}
