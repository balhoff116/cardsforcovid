package game

type whiteCard struct {
	Id   int    `json:"id"`
	Text string `json:"text"`
}

type blackCard struct {
	Id     int    `json:"id"`
	Text   string `json:"text"`
	Blanks int    `json:"blanks"`
}

func newWhiteCard(id int, text string) *whiteCard {
	return &whiteCard{
		Id:   id,
		Text: text,
	}
}

func newBlackCard(id int, text string, blanks int) *blackCard {
	return &blackCard{
		Id:     id,
		Text:   text,
		Blanks: blanks,
	}
}

func (b *blackCard) GetHandSize() int {
	totalHandSize := baseHandCount
	if b.Blanks == 3 {
		//draw 2, pick 3 rule. hand ends up being 12 cards
		totalHandSize += 2
	}

	return totalHandSize
}
