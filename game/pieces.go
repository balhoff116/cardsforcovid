package game

import (
	"encoding/json"
	"io/ioutil"
)

type GamePieces struct {
	Whites    []*whiteCard       `json:"whites"`
	Blacks    []*blackCard       `json:"blacks"`
	WhitesMap map[int]*whiteCard `json:"-"`
	BlacksMap map[int]*blackCard `json:"-"`
}

//types for json data coming from crhallberg
type hallbergBlackCard struct {
	Text string `json:"text"`
	Pick int    `json:"pick"`
}

type hallbergSet struct {
	BlackCards []*hallbergBlackCard `json:"blackCards"`
	WhiteCards []string             `json:"whiteCards"`
}

func NewGamePieces(filename string) (*GamePieces, error) {
	cards, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var hallberg hallbergSet
	err = json.Unmarshal(cards, &hallberg)
	if err != nil {
		return nil, err
	}

	whites := make([]*whiteCard, 0, len(hallberg.WhiteCards))
	whitesMap := make(map[int]*whiteCard)
	for ix, card := range hallberg.WhiteCards {
		whiteCard := newWhiteCard(ix, card)
		whites = append(whites, whiteCard)
		whitesMap[ix] = whiteCard
	}

	blacks := make([]*blackCard, 0, len(hallberg.BlackCards))
	blacksMap := make(map[int]*blackCard)
	for ix, card := range hallberg.BlackCards {
		blackCard := newBlackCard(ix, card.Text, card.Pick)
		blacks = append(blacks, blackCard)
		blacksMap[ix] = blackCard
	}

	return &GamePieces{
		Whites:    whites,
		Blacks:    blacks,
		WhitesMap: whitesMap,
		BlacksMap: blacksMap,
	}, nil
}
