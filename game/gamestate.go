package game

import (
	"github.com/google/uuid"
)

const (
	baseHandCount = 10
)

//TODO: make the judge an index in the game instance struct
type GameInstance struct {
	Players        []*Player   `json:"players"`
	BlackCardId    int         `json:"blackCardId"`
	Round          int         `json:"round"`
	PreviousRounds []GameRound `json:"previousRounds"` //oldest to newest, left to right
	//may evolve to more info. just clientId -> name now
	//this will also include players who have dropped
	PlayerNames   map[uuid.UUID]string `json:"playerNames"`
	whiteCardDeck *whiteCardDeck
	blackCardDeck *blackCardDeck
	GamePieces    *GamePieces
}

type GameRound struct {
	BlackCardId   int        `json:"blackCardId"`
	JudgeClientId uuid.UUID  `json:"judgeClientId"`
	WinnerIndex   int        `json:"winnerIndex"` //index in the Proposals array of the winner
	Proposals     []Proposal `json:"proposals"`
}

type Proposal struct {
	ClientId     uuid.UUID `json:"clientId"`
	WhiteCardIds []int     `json:"whiteCardIds"`
}

func NewGameInstance(gamePieces *GamePieces) *GameInstance {
	whiteCardDeck := newWhiteCardDeck(gamePieces.Whites)
	blackCardDeck := newBlackCardDeck(gamePieces.Blacks)

	return &GameInstance{
		Players:        nil,
		BlackCardId:    blackCardDeck.drawCard(),
		Round:          0,
		PreviousRounds: nil,
		PlayerNames:    make(map[uuid.UUID]string),
		whiteCardDeck:  whiteCardDeck,
		blackCardDeck:  blackCardDeck,
		GamePieces:     gamePieces,
	}
}

func (gi *GameInstance) NextRound(winnerId uuid.UUID) {
	gi.Round++
	gi.storeLastRound(winnerId)

	//discard the black card that was used for the previous round, and draw a new one
	gi.blackCardDeck.discardCard(gi.BlackCardId)
	gi.BlackCardId = gi.blackCardDeck.drawCard()

	//round robin the judge
	for ix, player := range gi.Players {
		if player.IsJudging {
			player.IsJudging = false
			gi.Players[(ix+1)%len(gi.Players)].IsJudging = true
			break
		}
	}

	//update each player
	for _, player := range gi.Players {
		player.nextRound(winnerId, gi.BlackCardId, gi.whiteCardDeck, gi.GamePieces)
	}
}

//This should be called in between rounds
func (gi *GameInstance) storeLastRound(winnerId uuid.UUID) {
	currentJudge := gi.findCurrentJudge()
	gameRound := GameRound{
		BlackCardId:   gi.BlackCardId,
		JudgeClientId: currentJudge.ClientId,
		Proposals:     make([]Proposal, 0, len(gi.Players)),
	}

	for ix, player := range gi.Players {
		newProposal := Proposal{
			ClientId:     player.ClientId,
			WhiteCardIds: make([]int, 0, len(player.CurrentProposal)),
		}

		newProposal.WhiteCardIds = append(newProposal.WhiteCardIds, player.CurrentProposal...)

		gameRound.Proposals = append(gameRound.Proposals, newProposal)

		if winnerId == player.ClientId {
			gameRound.WinnerIndex = ix
		}
	}

	gi.PreviousRounds = append(gi.PreviousRounds, gameRound)
}

func (gi *GameInstance) findCurrentJudge() *Player {
	for _, player := range gi.Players {
		if player.IsJudging {
			return player
		}
	}

	return nil //should not happen
}

func (gi *GameInstance) findPlayer(clientId uuid.UUID) *Player {
	for _, pl := range gi.Players {
		if pl.ClientId == clientId {
			return pl
		}
	}

	return nil //shouldn't happen
}

func (gi *GameInstance) AddPlayer(pl *Player) {
	gi.Players = append(gi.Players, pl)
}

func (gi *GameInstance) SetPlayerName(clientId uuid.UUID, name string) {
	player := gi.findPlayer(clientId)
	player.Name = name
	gi.PlayerNames[clientId] = name
}

func (gi *GameInstance) IsJudgeTime() bool {
	for _, pl := range gi.Players {
		if !pl.IsJudging && pl.CurrentProposal != nil {
			return false
		}
	}
	return true
}

func (gi *GameInstance) RemovePlayer(player *Player) {
	var ix int
	for ix = 0; ix < len(gi.Players); ix++ {
		if gi.Players[ix].ClientId == player.ClientId {
			break
		}
	}

	gi.Players[len(gi.Players)-1], gi.Players[ix] = gi.Players[ix], gi.Players[len(gi.Players)-1]
	gi.Players = gi.Players[:len(gi.Players)-1]
}

func (gi *GameInstance) DiscardAllProposals(player *Player) {
	for _, cardId := range player.CurrentProposal {
		gi.whiteCardDeck.discardCard(cardId)
		player.removeCardFromHand(cardId)
	}
	player.CurrentProposal = nil
}

func (gi *GameInstance) DeletePlayer(plIx int, player *Player) {
	//drop nonjudge
	//  discard all white cards
	//  remove player and client from lists
	//drop judge
	//  change black card
	//  for all clients
	//    if judge
	//      see "drop nonjudge"
	//      move the judge to the next guy
	//    else if current proposals were visible
	//      discard all proposal cards
	//    else
	//      add proposal cards back to the deck
	//    set the needed cards to the correct amount for the new black card
	//      the judge always should have 10
	//  increase round

	//start passing the round number when submitting a proposal so that you know if it's still a valid proposal (judge could have been dropped)
	if !player.IsJudging {
		//easy path. discard all the white cards. remove player and client from lists
		//h.discardAllCards(player)
		gi.whiteCardDeck.discardCards(player.Cards)
		gi.RemovePlayer(player)
	} else {
		areProposalsVisible := gi.IsJudgeTime()

		//discard black card
		gi.blackCardDeck.discardCard(gi.BlackCardId)
		gi.BlackCardId = gi.blackCardDeck.drawCard()

		//get new hand size
		handSize := gi.GamePieces.BlacksMap[gi.BlackCardId].GetHandSize()

		for _, pl := range gi.Players {
			if !pl.IsJudging {
				if areProposalsVisible {
					//discard all of the proposal cards
					gi.DiscardAllProposals(pl)
				}

				//drop the current proposal
				pl.CurrentProposal = nil

				//set the correct hand size
				if len(pl.Cards) > handSize {
					//discard the extra cards and remove them from the player's hand
					//TODO: BUG. use handSize, not baseHandCount *forehead*
					gi.whiteCardDeck.discardCards(pl.Cards[handSize:])
					pl.Cards = pl.Cards[:handSize]
				} else {
					//add more cards
					for len(pl.Cards) < handSize {
						pl.Cards = append(pl.Cards, gi.whiteCardDeck.drawCard())
					}
				}
			}
		}

		//consolidate with the code above??
		//drop the judge
		gi.whiteCardDeck.discardCards(player.Cards)
		gi.RemovePlayer(player)
		//set the new judge
		if len(gi.Players) > 0 {
			gi.Players[plIx%len(gi.Players)].IsJudging = true
		}
		//increase round
		gi.Round++
	}
}
