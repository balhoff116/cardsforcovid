package game

import (
	"math/rand"
	"time"
)

type whiteCardDeck struct {
	currentDeck []int
	nextDeck    []int
}

type blackCardDeck struct {
	currentDeck []int
	nextDeck    []int
}

func newWhiteCardDeck(cards []*whiteCard) *whiteCardDeck {
	currentDeck := mapWhiteCards(cards, func(card *whiteCard) int {
		return card.Id
	})

	deck := &whiteCardDeck{
		currentDeck: currentDeck,
		nextDeck:    make([]int, 0, len(cards)),
	}

	deck.shuffle()
	return deck
}

//shuffle will just shuffle the currentDeck, not the next deck
func (d *whiteCardDeck) shuffle() {
	//todo: seed it every time? is this thread based?
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(d.currentDeck),
		func(i, j int) { d.currentDeck[i], d.currentDeck[j] = d.currentDeck[j], d.currentDeck[i] })
}

func (d *whiteCardDeck) drawCard() (cardId int) {
	if len(d.currentDeck) == 0 {
		d.currentDeck = d.nextDeck
		d.nextDeck = make([]int, 0, len(d.currentDeck))
		d.shuffle()
	}

	cardId = d.currentDeck[0]
	d.currentDeck = d.currentDeck[1:]

	return
}

func (d *whiteCardDeck) discardCard(cardId int) {
	d.nextDeck = append(d.nextDeck, cardId)
}

func (d *whiteCardDeck) discardCards(cardIds []int) {
	for _, id := range cardIds {
		d.discardCard(id)
	}
}

func mapWhiteCards(in []*whiteCard, f func(*whiteCard) int) []int {
	out := make([]int, len(in))
	for ix, v := range in {
		out[ix] = f(v)
	}
	return out
}

func newBlackCardDeck(cards []*blackCard) *blackCardDeck {
	currentDeck := mapBlackCards(cards, func(card *blackCard) int {
		return card.Id
	})

	deck := &blackCardDeck{
		currentDeck: currentDeck,
		nextDeck:    make([]int, 0, len(cards)),
	}

	deck.shuffle()
	return deck
}

//shuffle will just shuffle the currentDeck, not the next deck
func (d *blackCardDeck) shuffle() {
	//todo: seed it every time? is this thread based?
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(d.currentDeck),
		func(i, j int) { d.currentDeck[i], d.currentDeck[j] = d.currentDeck[j], d.currentDeck[i] })
}

func (d *blackCardDeck) drawCard() (cardId int) {
	if len(d.currentDeck) == 0 {
		d.currentDeck = d.nextDeck
		d.nextDeck = make([]int, 0, len(d.currentDeck))
		d.shuffle()
	}

	cardId = d.currentDeck[0]
	d.currentDeck = d.currentDeck[1:]

	return
}

func (d *blackCardDeck) discardCard(cardId int) {
	d.nextDeck = append(d.nextDeck, cardId)
}

func mapBlackCards(in []*blackCard, f func(*blackCard) int) []int {
	out := make([]int, len(in))
	for ix, v := range in {
		out[ix] = f(v)
	}
	return out
}
