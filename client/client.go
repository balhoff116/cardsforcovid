package client

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	msg "bitbucket.org/balhoff116/cardsforcovid/message"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

//TODO: This is just defined everywhere i guess
const defaultRoomID = "1"

type ClientChans struct {
	Register   chan RegisterData
	Unregister chan RegisterData
	Submit     chan SubmitWinnerPayload
	Propose    chan SubmitProposalPayload
	Name       chan SetNamePayload
}

type RegisterData struct {
	RoomID string
	Client *Client
}

func NewClientChans() ClientChans {
	return ClientChans{
		Register:   make(chan RegisterData),
		Unregister: make(chan RegisterData),
		Submit:     make(chan SubmitWinnerPayload),
		Propose:    make(chan SubmitProposalPayload),
		Name:       make(chan SetNamePayload),
	}
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 4096

	commandKey            = "command"
	payloadKey            = "payload"
	setNameCommand        = "setName"
	submitProposalCommand = "submitProposal"
	submitWinnerCommand   = "submitWinner"
)

//TODO: I don't like that these aren't in message, but what to do
type SetNamePayload struct {
	Client  Client
	Payload msg.SetNameObj
}

type SubmitProposalPayload struct {
	Client  Client
	Payload msg.SubmitProposalObj
}

type SubmitWinnerPayload struct {
	Client  Client
	Payload msg.SubmitWinnerObj
}

type commandObj struct {
	Command string          `json:"command"`
	Payload json.RawMessage `json:"payload"`
}

type Client struct {
	Chans ClientChans
	Conn  *websocket.Conn
	Send  chan []byte
	ID    uuid.UUID
}

func (c *Client) Start() {
	go c.writePump()
	go c.readPump()
}

func (c *Client) readPump() {
	defer func() {
		c.Chans.Unregister <- RegisterData{defaultRoomID, c}
		c.Conn.Close()
	}()

	c.Conn.SetReadLimit(maxMessageSize)
	c.Conn.SetReadDeadline(time.Now().Add(pongWait))
	c.Conn.SetPongHandler(func(string) error {
		c.Conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, message, err := c.Conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("Error reading message: %v\n", err)
			}
			break
		}

		fmt.Printf("Received: %s\n", message)
		c.processIncoming(message)
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.Conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.Send:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.Conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.Conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.Conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				fmt.Printf("Error writing ping message: %v\n", err)
				return
			}
		}
	}
}

func (c *Client) processIncoming(message []byte) error {
	var commandObj commandObj
	err := json.Unmarshal(message, &commandObj)

	if err != nil {
		log.Printf("error processing client message: %v\n", err)
		return err
	}

	switch command := commandObj.Command; command {
	case setNameCommand:
		var setName msg.SetNameObj
		err = json.Unmarshal(commandObj.Payload, &setName)
		if err != nil {
			log.Printf("error processing client message: %v\n", err)
			return err
		}
		c.processSetNameCommand(&setName)
	case submitProposalCommand:
		var submitProposal msg.SubmitProposalObj
		err = json.Unmarshal(commandObj.Payload, &submitProposal)
		if err != nil {
			log.Printf("error processing client message: %v\n", err)
			return err
		}
		c.processSubmitProposalCommand(&submitProposal)
	case submitWinnerCommand:
		var submitWinner msg.SubmitWinnerObj
		err = json.Unmarshal(commandObj.Payload, &submitWinner)
		if err != nil {
			log.Printf("error processing client message: %v\n", err)
			return err
		}
		c.processSubmitWinnerCommand(&submitWinner)
	}

	return nil
}

func (c Client) processSetNameCommand(payload *msg.SetNameObj) {
	c.Chans.Name <- SetNamePayload{
		Client:  c,
		Payload: *payload,
	}
}

func (c Client) processSubmitProposalCommand(payload *msg.SubmitProposalObj) {
	c.Chans.Propose <- SubmitProposalPayload{
		Client:  c,
		Payload: *payload,
	}
}

func (c Client) processSubmitWinnerCommand(payload *msg.SubmitWinnerObj) {
	c.Chans.Submit <- SubmitWinnerPayload{
		Client:  c,
		Payload: *payload,
	}
}
