package message

import (
	"bitbucket.org/balhoff116/cardsforcovid/game"
	"github.com/google/uuid"
)

const (
	initCommandKey  = "init"
	stateCommandKey = "state"
)

type BroadcastData struct {
	RoomID  string
	Payload []byte
}

type initClientMessage struct {
	Command string             `json:"command"`
	Payload *initClientPayload `json:"payload"`
}

type initClientPayload struct {
	ClientId   uuid.UUID        `json:"clientId"`
	GamePieces *game.GamePieces `json:"gamePieces"`
}

type stateMessage struct {
	Command string             `json:"command"`
	Payload *game.GameInstance `json:"payload"`
}

func NewInitClientMessage(clientID uuid.UUID, gamePieces *game.GamePieces) *initClientMessage {
	return &initClientMessage{
		Command: initCommandKey,
		Payload: &initClientPayload{
			ClientId:   clientID,
			GamePieces: gamePieces,
		},
	}
}

func NewStateMessage(payload *game.GameInstance) *stateMessage {
	return &stateMessage{
		Command: stateCommandKey,
		Payload: payload,
	}
}

type SetNameObj struct {
	Name string `json:"name"`
}

type SubmitProposalObj struct {
	Round   int   `json:"round"`
	CardIds []int `json:"cardIds"`
}

type SubmitWinnerObj struct {
	PlayerId uuid.UUID `json:"playerId"`
}
